﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function deleteEntity(id) {
    $.ajax({
        method: 'POST',
        url: '/home/Delete',
        data: { id: id },
        success: location.replace('home/index')
    });
}
function returnTask(id, name, desc) {
    var model = { Id: id, Name: name, Description: desc };
    return model;
}