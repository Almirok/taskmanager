﻿using Microsoft.EntityFrameworkCore;

namespace TaskManagerCore.Models
{
    public sealed class CommonDbContext : DbContext
    {
        public DbSet<TaskModel> TaskModels { get; set; }

        public CommonDbContext(DbContextOptions<CommonDbContext> options) : base(options)
        {
            if (!Database.IsInMemory())
            {
                Database.EnsureCreated();
            }
        }
    }
}
