﻿using System;
using TaskManagerCore.Interfaces;

namespace TaskManagerCore.Models
{
    public class TaskModel : IModelEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TimeSpan? Time { get; set; }
    }
}
