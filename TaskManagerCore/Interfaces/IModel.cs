﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManagerCore.Interfaces
{
    public interface IModelEntity
    {
        long Id { get; set; }
    }
}
