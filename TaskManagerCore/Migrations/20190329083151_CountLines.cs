﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TaskManagerCore.Migrations
{
    public partial class CountLines : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CountLines",
                table: "TaskModels",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CountLines",
                table: "TaskModels");
        }
    }
}
