﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using TaskManagerCore.Interfaces;
using TaskManagerCore.Models;

namespace TaskManagerCore.Helpers
{
    public class DataHelper
    {
        public static async Task<bool> SaveOrEditEntityAsync<T>(CommonDbContext ctx, T entity) where T : class, IModelEntity
        {
            return await ProbablyWrongActionAsync(() =>
            {
                if (ctx.Set<T>().Any(x => x.Id == entity.Id))
                {
                    ctx.Set<T>().Attach(entity);
                    ctx.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    ctx.Set<T>().Add(entity);
                    ctx.SaveChanges();
                }
            });
        }

        public static async Task<IModelEntity> GetTaskByIdAsync<T>(CommonDbContext ctx, long? entityId) where T : class, IModelEntity
        {
            return await ProbablyWrongActionAsync(() =>
            {
                var entity = ctx.Set<T>().Find(entityId);
                return entity;
            });
        }

        public static async Task<bool> SaveOrEditEntityByIdAsync<T>(CommonDbContext ctx, long? entityId) where T : class, IModelEntity
        {
            return await ProbablyWrongActionAsync(() =>
            {
                var entity = ctx.Set<T>().Find(entityId);
                if (ctx.Set<T>().Any(x => x.Id == entity.Id))
                {
                    ctx.Set<T>().Attach(entity);
                    ctx.Entry(entity).State = EntityState.Modified;
                }
                else
                {
                    ctx.Set<T>().Add(entity);
                    ctx.SaveChanges();
                }
            });
        }

        public static async Task<bool> ProbablyWrongActionAsync(Action action)
        {
            return await Task.Factory.StartNew(() =>
            {
                try
                {
                    action?.Invoke();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }

        public static async Task<IModelEntity> ProbablyWrongActionAsync(Func<IModelEntity> action)
        {
            return await Task.Factory.StartNew(() =>
            {
                try
                {
                    return action?.Invoke();
                }
                catch (Exception)
                {
                    return null;
                }
            });
        }

        public static async Task<bool> DeleteEntityAsync<T>(CommonDbContext ctx, T entity) where T : class, IModelEntity
        {
            return await ProbablyWrongActionAsync(() =>
            {
                ctx.Set<T>().Attach(entity);
                ctx.Entry(entity).State = EntityState.Deleted;
                ctx.SaveChanges();
            });
        }

        public static async Task<bool> DeleteEntityByIdAsync<T>(CommonDbContext ctx, long? entityId) where T : class, IModelEntity
        {
            return await ProbablyWrongActionAsync(() =>
            {
                var entity = ctx.Set<T>().Find(entityId);
                ctx.Set<T>().Attach(entity);
                ctx.Entry(entity).State = EntityState.Deleted;
                ctx.SaveChanges();
            });
        }
    }
}
