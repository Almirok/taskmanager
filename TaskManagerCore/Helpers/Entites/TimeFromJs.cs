﻿using System;

namespace TaskManagerCore.Helpers.Entites
{
    public class TimeFromJs
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public int Second { get; set; }

        public TimeSpan GetTimeSpan()
        {
            return new TimeSpan(Hours, Minutes, Second);
        }
    }
}
