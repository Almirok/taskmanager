﻿using System;
using TaskManagerCore.Helpers.Entites;

namespace TaskManagerCore.Helpers
{
    public static class ConvertHelper
    {
        /// <summary>
        /// Get time from ajax request with main page
        /// </summary>
        /// <param name="ajaxTime">Value signature is (00:00:00)</param>
        /// <returns></returns>
        public static TimeSpan? GetTimeSpanFromAjaxString(string ajaxTime)
        {
            TimeFromJs time = new TimeFromJs();
            var splitStringToTime = ajaxTime.Split(':');
            if (splitStringToTime.Length != 3)
            {
                time.Hours = int.Parse(splitStringToTime[0]);
                time.Minutes = int.Parse(splitStringToTime[1]);
                time.Second = int.Parse(splitStringToTime[2]);
                return time.GetTimeSpan();
            }

            return null;
        }
    }
}
