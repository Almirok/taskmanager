﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Threading.Tasks;
using TaskManagerCore.Helpers;
using TaskManagerCore.Interfaces;
using TaskManagerCore.Models;

namespace TaskManagerCore.Controllers
{
    public class HomeController : Controller
    {
        private CommonDbContext _ctx;

        public HomeController(CommonDbContext context)
        {
            _ctx = context;
        }

        public async Task<IActionResult> Index()
        {
            var tasks = await _ctx.TaskModels.ToListAsync();

            return View(tasks);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id != null)
            {
                TaskModel task = await _ctx.TaskModels.FirstOrDefaultAsync(p => p.Id == id);
                if (task != null)
                    return View(task);
            }
            return NotFound();
        }

        [HttpPost]
        public async Task<IActionResult> Delete(long? id)
        {
            if(id!= null) await DataHelper.DeleteEntityByIdAsync<TaskModel>(_ctx, id); 
            return await Index();
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(TaskModel task)
        {
            await DataHelper.SaveOrEditEntityAsync(_ctx, task);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(long id)
        {
            var entity = _ctx.TaskModels.Find(id);
            return View(entity);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(TaskModel task)
        {
            await DataHelper.SaveOrEditEntityAsync(_ctx, task);
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public async Task<IActionResult> SetTime(int? data, long? id)
        {
            if (data != null)
            {
                var taskTime = TimeSpan.FromSeconds(data.Value);
                var task = await DataHelper.GetTaskByIdAsync<TaskModel>(_ctx, id) as TaskModel;
                if (task != null) task.Time = taskTime;
                await DataHelper.SaveOrEditEntityAsync(_ctx, task);
            }
            await _ctx.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
